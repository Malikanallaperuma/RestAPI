var express    = require('express');        // call express
var app        = express();                 // define our app using express
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var User     = require('./app/models/users');


app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());


var port=process.env.PORT || 8080;
mongoose.connect('mongodb://abiling_db_user:#dsound@ds145138.mlab.com:45138/abiling');

var router = express.Router(); //get an instance of express router

// middleware to use for all requests

router.use(function(req,res,next){

	console.log('happening it');

	next();
});

//test route

router.get('/',function(req,res){

	res.json({message :'api working..!!'});
});

router.route('/users')

 // create a User (accessed at POST http://localhost:8080/api/users)

 .post(function(req, res){

 	var user = new User();
 	user.name = req.body.name;

 	//save user and check for errors

 	user.save(function(err){

 		if(err)


 		res.send(err);
 	res.json({message:'User Created..!'});

 	
 	});

 })


//Register routes

app.use('/api',router);

//START server

app.listen(port);
console.log('testing in port:' +port);
